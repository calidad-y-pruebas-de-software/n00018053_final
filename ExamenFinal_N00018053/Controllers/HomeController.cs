﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ExamenFinal_N00018053.Models;

namespace ExamenFinal_N00018053.Controllers
{
    public class HomeController : Controller
    {
        List<Jugador> jugadores = new List<Jugador>();
        List<Carta> mazo = new List<Carta>();
        Random rnd = new Random();
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Jugar(List<string> Jugadores)
        {
            for (int i=0;i<5;i++)
            {
                var jugador = new Jugador();
                jugador.nombre = Jugadores[i];
                jugadores.Add(jugador);
            }
            repartirCartas(jugadores);

            return View(jugadores);
        }

        public void mazoCartas()
        {
            var carta1 = new Carta() { numero = 2, palo = 1 }; mazo.Add(carta1);
            var carta2 = new Carta() { numero = 3, palo = 1 }; mazo.Add(carta2);
            var carta3 = new Carta() { numero = 4, palo = 1 }; mazo.Add(carta3);
            var carta4 = new Carta() { numero = 5, palo = 1 }; mazo.Add(carta4);
            var carta5 = new Carta() { numero = 6, palo = 1 }; mazo.Add(carta5);
            var carta6 = new Carta() { numero = 7, palo = 1 }; mazo.Add(carta6);
            var carta7 = new Carta() { numero = 8, palo = 1 }; mazo.Add(carta7);
            var carta8 = new Carta() { numero = 9, palo = 1 }; mazo.Add(carta8);
            var carta9 = new Carta() { numero = 10, palo = 1 }; mazo.Add(carta9);
            var carta10 = new Carta() { numero = 11, palo = 1 }; mazo.Add(carta10);
            var carta11 = new Carta() { numero = 12, palo = 1 }; mazo.Add(carta11);
            var carta12 = new Carta() { numero = 13, palo = 1 }; mazo.Add(carta12);
            var carta13 = new Carta() { numero = 14, palo = 1 }; mazo.Add(carta13);
            var carta14 = new Carta() { numero = 2, palo = 2 }; mazo.Add(carta14);
            var carta15 = new Carta() { numero = 3, palo = 2 }; mazo.Add(carta15);
            var carta16 = new Carta() { numero = 4, palo = 2 }; mazo.Add(carta16);
            var carta17 = new Carta() { numero = 5, palo = 2 }; mazo.Add(carta17);
            var carta18 = new Carta() { numero = 6, palo = 2 }; mazo.Add(carta18);
            var carta19 = new Carta() { numero = 7, palo = 2 }; mazo.Add(carta19);
            var carta20 = new Carta() { numero = 8, palo = 2 }; mazo.Add(carta20);
            var carta21 = new Carta() { numero = 9, palo = 2 }; mazo.Add(carta21);
            var carta22 = new Carta() { numero = 10, palo = 2 }; mazo.Add(carta22);
            var carta23 = new Carta() { numero = 11, palo = 2 }; mazo.Add(carta23);
            var carta24 = new Carta() { numero = 12, palo = 2 }; mazo.Add(carta24);
            var carta25 = new Carta() { numero = 13, palo = 2 }; mazo.Add(carta25);
            var carta26 = new Carta() { numero = 14, palo = 2 }; mazo.Add(carta26);
            var carta27 = new Carta() { numero = 2, palo = 3 }; mazo.Add(carta27);
            var carta28 = new Carta() { numero = 3, palo = 3 }; mazo.Add(carta28);
            var carta29 = new Carta() { numero = 4, palo = 3 }; mazo.Add(carta29);          
            var carta30 = new Carta() { numero = 5, palo = 3 }; mazo.Add(carta30);
            var carta31 = new Carta() { numero = 6, palo = 3 }; mazo.Add(carta31);
            var carta32 = new Carta() { numero = 7, palo = 3 }; mazo.Add(carta32);
            var carta33 = new Carta() { numero = 8, palo = 3 }; mazo.Add(carta33);
            var carta34 = new Carta() { numero = 9, palo = 3 }; mazo.Add(carta34);
            var carta35 = new Carta() { numero = 10, palo = 3 }; mazo.Add(carta35);
            var carta36 = new Carta() { numero = 11, palo = 3 }; mazo.Add(carta36);
            var carta37 = new Carta() { numero = 12, palo = 3 }; mazo.Add(carta37);
            var carta38 = new Carta() { numero = 13, palo = 3 }; mazo.Add(carta38);
            var carta39 = new Carta() { numero = 14, palo = 3 }; mazo.Add(carta39);
            var carta40 = new Carta() { numero = 2, palo = 4 }; mazo.Add(carta40);
            var carta41 = new Carta() { numero = 3, palo = 4 }; mazo.Add(carta41);
            var carta42 = new Carta() { numero = 4, palo = 4 }; mazo.Add(carta42);
            var carta43 = new Carta() { numero = 5, palo = 4 }; mazo.Add(carta43);
            var carta44 = new Carta() { numero = 6, palo = 4 }; mazo.Add(carta44);
            var carta45 = new Carta() { numero = 7, palo = 4 }; mazo.Add(carta45);
            var carta46 = new Carta() { numero = 8, palo = 4 }; mazo.Add(carta46);
            var carta47 = new Carta() { numero = 9, palo = 4 }; mazo.Add(carta47);
            var carta48 = new Carta() { numero = 10, palo = 4 }; mazo.Add(carta48);
            var carta49 = new Carta() { numero = 11, palo = 4 }; mazo.Add(carta49);
            var carta50 = new Carta() { numero = 12, palo = 4 }; mazo.Add(carta50);
            var carta51 = new Carta() { numero = 13, palo = 4 }; mazo.Add(carta51);
            var carta52 = new Carta() { numero = 14, palo = 4 }; mazo.Add(carta52);
        }

        public void repartirCartas(List<Jugador> jugadores)
        {
            mazoCartas();

            for (int i=0;i<5;i++)
            {
                jugadores[i].mano = generarMano();
            }
        }

        public List<Carta> generarMano()
        {
            List<Carta> mano = new List<Carta>();
            int i = 0;
            while (i<5)
            {
                int index = rnd.Next(mazo.Count);
                mano.Add(mazo[index]);
                mano = mano.OrderBy(o => o.numero).ToList();
                mazo.RemoveAt(index);
                i++;
            }
            return mano;
        }
        public string escaleraDeColor(List<Carta> cartas)
        {
            Carta cartaT = new Carta();

            int i = 0;

            foreach (var carta in cartas)
            {

                if (cartaT.numero + 1 == carta.numero && cartaT.palo == carta.palo)
                {
                    cartaT = carta;
                    i++;
                }
                if (i == 0)
                {
                    cartaT = carta;
                    i++;
                }
            }
            if (i == 5)
            {
                return "Escalera de color";
            }

            return "No es Escalera de color";
        }

        public string poker(List<Carta> cartas)
        {
            Carta cartaT = new Carta();

            int i = 0;

            foreach (var carta in cartas)
            {

                if (cartaT.numero == carta.numero && i<4)
                {
                    cartaT = carta;
                    i++;
                }
                else
                {
                    cartaT = carta;
                    i = 1;
                }
            }
            if (i == 4)
            {
                return "Poker";
            }

            return "No es Poker";
        }

        public string full(List<Carta> cartas)
        {
            Carta cartaT = new Carta();
            Carta cartat2 = new Carta();

            int i = 0;
            int j = 0;

            foreach (var carta in cartas)
            {
                if (i<3) { 
                    if (cartaT.numero == carta.numero)
                    {
                        i++;
                    }
                    else 
                    {
                        cartaT = carta;
                        i = 1;
                    }
                    if (i == 0)
                    {
                        cartaT = carta;
                        i++;
                    }
                }
                if (i >=2)
                {
                    if (j < 3) { 
                        if (cartat2.numero == carta.numero)
                        {
                            j++;
                        }
                        else
                        {
                            cartat2 = carta;
                            j = 1;
                        }
                        if (j == 0)
                        {
                            cartat2 = carta;
                            j++;
                        }

                    }
                }

            }
            if (i == 3 && j == 2)
            {
                return "Es Full";
            }
            if (i == 2 && j == 3 )
            {
                return "Es Full";
            }
            return "";
        }
        public string esColor(List<Carta> cartas)
        {
            Carta cartaT = new Carta();

            int i = 0;

            foreach (var carta in cartas)
            {

                if (cartaT.palo == carta.palo)
                {
                    cartaT = carta;
                    i++;
                }
                if (i == 0)
                {
                    cartaT = carta;
                    i++;
                }
            }
            if (i == 5)
            {
                return "Es Color";
            }

            return "";
        }

        public string esEscaleraConciderandoA(List<Carta> cartas)
        {
            Carta cartaT = new Carta();

            int i = 0;

            foreach (var carta in cartas)
            {
                if (cartaT.numero + 1 == carta.numero)
                {
                    cartaT = carta;
                    i++;
                }
                else if (carta.numero == 14 && cartaT.numero == 5)
                {
                    i++;
                }
                else
                {
                    cartaT = carta;
                    i = 1;
                }
                if (i == 0)
                {
                    cartaT = carta;
                    i++;
                }
            }
            if (i == 5)
            {
                return "Es Escalera";
            }

            return "";
        }
        public string Trio(List<Carta> cartas)
        {
            Carta cartat = new Carta();

            int i = 0;
            int j = 0;

            foreach (var carta in cartas)
            {
                if (cartat.numero == carta.numero)
                {
                    cartat = carta;
                    if (i < 3)
                    {
                        i++;
                    }
                    else if (i >= 3)
                    {
                        j++;
                    }
                }
                if (i == 0)
                {
                    cartat = carta; i++; j++;
                }
                if (i == 3)
                {
                    cartat = carta;
                }
                if (cartat.numero != carta.numero)
                {
                    cartat = carta;
                    i = 1;
                }

            }
            if (i == 3 && j == 1)
            {
                return "Es Trio";
            }
            else
            {
                return "";
            }
        }
        public string DoblePar(List<Carta> cartas)
        {
            Carta cartaT = new Carta();
            
            int i = 0;
            int j = 0;
            int k = 0;

            foreach (var carta in cartas)
            {
                if (cartaT.numero == carta.numero)
                {
                    cartaT = carta;
                    if (k == 0)
                    {
                        i++;
                    }
                    else if (k == 1)
                    {
                        j++;
                    }
                }
                if (i == 0)
                {
                    cartaT = carta; i++; j++;
                }
                if (cartaT.numero != carta.numero)
                {
                    k = 1;
                    cartaT = carta;
                }
            }
            if (i == 2 && j == 2)
            {
                return "Es Doble par";
            }
            else
            {
                return "";
            }
        }
        public string UnPar(List<Carta> cartas)
        {
            Carta cartaT = new Carta();
            
            int i = 0;
            int j = 0;
            int k = 0;
            foreach (var carta in cartas)
            {
                if (cartaT.numero == carta.numero)
                {
                    cartaT = carta;
                    if (k == 0)
                    {
                        i++;
                    }
                    else if (k == 1)
                    {
                        j++;
                    }
                }
                if (i == 0)
                {
                    cartaT = carta; i++; j++;
                }
                if (cartaT.numero != carta.numero)
                {
                    k = 1;
                    cartaT = carta;
                }
            }
            if ((i == 2 && j == 1) || (i == 1 && j == 2))
            {
                return "Es Un Par";
            }
            else
            {
                return "";
            }
        }
        
    }
}

