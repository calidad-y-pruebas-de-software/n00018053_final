﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinal_N00018053.Models
{
    public class Jugador
    {
        public string nombre { get; set; }
        public List<Carta> mano { get; set; }
        public string tipoMano{ get; set; }
    }
}
