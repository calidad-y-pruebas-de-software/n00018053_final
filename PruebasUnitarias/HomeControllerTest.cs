using ExamenFinal_N00018053.Controllers;
using ExamenFinal_N00018053.Models;
using NUnit.Framework;
using System.Collections.Generic;

namespace PruebasUnitarias
{
    public class HomeControllerTest
    {

        [Test]
        public void EsEscaleradecolorManoInvencible()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 10, palo = 1});
            cartas.Add(new Carta() { numero = 11, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.escaleraDeColor(cartas);

            Assert.AreEqual("Escalera de color", resultado);
        }

        [Test]
        public void NoEsEscaleradecolorManoInvenciblePaloDistinto()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 11, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 2 });
            cartas.Add(new Carta() { numero = 13, palo = 2 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.escaleraDeColor(cartas);

            Assert.AreEqual("No es Escalera de color", resultado);
        }

        [Test]
        public void NoEsEscaleradecolor()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.escaleraDeColor(cartas);

            Assert.AreEqual("No es Escalera de color", resultado);
        }

        [Test]
        public void espokerMano()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 2 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 4 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.poker(cartas);

            Assert.AreEqual("Poker", resultado);
        }


        [Test]
        public void noesPokerMano()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 2 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.poker(cartas);

            Assert.AreEqual("No es Poker", resultado);
        }

        [Test]
        public void fullMano1()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 4 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.full(cartas);

            Assert.AreEqual("Es Full", resultado);
        }

        [Test]
        public void fullMano2()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 4 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.full(cartas);

            Assert.AreEqual("Es Full", resultado);
        }

        [Test]
        public void NoesfullMano()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 4 });
            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.full(cartas);

            Assert.AreEqual("", resultado);
        }

        [Test]
        public void EsColorMano()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.esColor(cartas);

            Assert.AreEqual("Es Color", resultado);
        }
        [Test]
        public void NoEsColorMano()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });

            var controller = new HomeController();

            var resultado = controller.esColor(cartas);

            Assert.AreEqual("", resultado);
        }

        [Test]
        public void EsEscalera()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 4 });
            cartas.Add(new Carta() { numero = 4, palo = 3 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });

            var controller = new HomeController();

            var resultado = controller.esEscaleraConciderandoA(cartas);

            Assert.AreEqual("Es Escalera", resultado);
        }

        [Test]
        public void EsEscaleraConAComoUno()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 2, palo = 4 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 1 });
            cartas.Add(new Carta() { numero = 5, palo = 2 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.esEscaleraConciderandoA(cartas);

            Assert.AreEqual("Es Escalera", resultado);
        }

        [Test]
        public void EsEscaleraConAComoCatorce()
        {

            List<Carta> cartas = new List<Carta>();
            //Cartas ordenadas
            cartas.Add(new Carta() { numero = 10, palo = 4 });
            cartas.Add(new Carta() { numero = 11, palo = 3 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 13, palo = 2 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.esEscaleraConciderandoA(cartas);

            Assert.AreEqual("Es Escalera", resultado);
        }

        [Test]
        public void esTrioAlInicio()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 7, palo = 2 });
            cartas.Add(new Carta() { numero = 9, palo = 3 });


            var controller = new HomeController();
            var resultado = controller.Trio(cartas);

            Assert.AreEqual("Es Trio", resultado);
        }

        [Test]
        public void esTrioAlFinal()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });


            var controller = new HomeController();
            var resultado = controller.Trio(cartas);

            Assert.AreEqual("Es Trio", resultado);
        }

        [Test]
        public void NoesTrio()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 8, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 4 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Trio(cartas);

            Assert.AreEqual("", resultado);
        }

        [Test]
        public void esDoblePar()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 4 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("Es Doble par", resultado);
        }

        [Test]
        public void esDobleParNoConsecutivo()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 4 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("Es Doble par", resultado);
        }


        [Test]
        public void NoEsDoblePar()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 4 });
            cartas.Add(new Carta() { numero = 2, palo = 3 });
            cartas.Add(new Carta() { numero = 7, palo = 2 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("", resultado);
        }

        [Test]
        public void EsUnPar()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 3 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.UnPar(cartas);

            Assert.AreEqual("Es Un Par", resultado);
        }


        [Test]
        public void NoEsUnPar()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });
            cartas.Add(new Carta() { numero = 9, palo = 4 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.UnPar(cartas);

            Assert.AreEqual("", resultado);
        }

    }
}